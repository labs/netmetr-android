package at.alladin.rmbt.android.test;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TemporaryStoredResults extends SQLiteOpenHelper {
	
	public static final String 		TABLE_OFFLINE_TESTS 	= "results";
	public static final String 		COLUMN_ID 				= "_id";
	public static final String 		COLUMN_REQUEST 			= "request";
	public static final String 		COLUMN_RESULT 			= "result";
	public static final String 		COLUMN_TIME 			= "time";

	private static final String 	DATABASE_NAME 			= "tests.db";
	private static final int 		DATABASE_VERSION 		= 1;

	private static final String 	DATABASE_CREATE = "CREATE TABLE " + TABLE_OFFLINE_TESTS + "(" 
			  										+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			  										+ COLUMN_REQUEST + " TEXT NOT NULL, "
			  										+ COLUMN_RESULT + " TEXT NOT NULL, "
			  										+ COLUMN_TIME + " INTEGER NOT NULL);";

	public TemporaryStoredResults(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
	}
	
	public void storeResult(String requestToStore, String resultToStore, long time) {
    	ContentValues contentValues = new ContentValues();
    	contentValues.put(TemporaryStoredResults.COLUMN_REQUEST, requestToStore);
    	contentValues.put(TemporaryStoredResults.COLUMN_RESULT, resultToStore);
    	contentValues.put(TemporaryStoredResults.COLUMN_TIME, time);
    	getWritableDatabase().insert(TemporaryStoredResults.TABLE_OFFLINE_TESTS, null, contentValues);
	}
	
	public Map<Integer, JSONObject[]> getStoredResults() {
		Map<Integer, JSONObject[]> storedResults = new HashMap<Integer, JSONObject[]>();
		
	Cursor cursor = getWritableDatabase().query(TemporaryStoredResults.TABLE_OFFLINE_TESTS, new String[]{TemporaryStoredResults.COLUMN_ID, TemporaryStoredResults.COLUMN_REQUEST, TemporaryStoredResults.COLUMN_RESULT, TemporaryStoredResults.COLUMN_TIME}, null, null, null, null, null);
    	cursor.moveToFirst();
    	while (!cursor.isAfterLast()) {
    		Integer id = cursor.getInt(0);
    		JSONObject storedRequest;
    		JSONObject storedResult;
			try {
				storedRequest = new JSONObject(cursor.getString(1));
				storedResult = new JSONObject(cursor.getString(2));
				storedResult.put("time", cursor.getString(3));
				storedResults.put(id, new JSONObject[] {storedRequest, storedResult});
				System.out.println("STORED RESULT: " + storedResult);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		cursor.moveToNext();
    	}
    	cursor.close();
    	
    	return storedResults;
	}
	
	public void deleteStoredResult(Integer id) {
		getWritableDatabase().delete(TemporaryStoredResults.TABLE_OFFLINE_TESTS, TemporaryStoredResults.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
	}
	
	public void deleteAllResults() {
    	getWritableDatabase().execSQL("DELETE FROM " + TABLE_OFFLINE_TESTS);
	}
}
