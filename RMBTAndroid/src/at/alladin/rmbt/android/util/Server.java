package at.alladin.rmbt.android.util;


public class Server {
    private static final String ENCODING_FORMAT_SPLIT = ";";
    private static final String ENCODING_FORMAT =       "%s" + ENCODING_FORMAT_SPLIT +
                                                        "%s" + ENCODING_FORMAT_SPLIT +
                                                        "%s" + ENCODING_FORMAT_SPLIT +
                                                        "%s" + ENCODING_FORMAT_SPLIT;
    private static final int    ENCODING_DATA_LENGTH  = 4;

    private static final String LOCATION_FORMAT       = "%s, %s";

    protected final String uuid;
    protected final String name;
    protected final String city;
    protected final String country;

    public Server(String uuid, String name, String city, String country) {
        this.uuid = uuid;
        this.name = name;
        this.city = city;
        this.country = country;
    }

    public String encodeToString() {
        return String.format(ENCODING_FORMAT, uuid, name, city, country);
    }

    public static Server decodeFromString(String data) {
        final String[] splitResult = data.split(ENCODING_FORMAT_SPLIT);

        if(splitResult.length < ENCODING_DATA_LENGTH) return null;

        return new Server(splitResult[0], splitResult[1], splitResult[2], splitResult[3]);
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getFormattedLocation() {
        return String.format(LOCATION_FORMAT, getCity(), getCountry());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null)    ? 0 : name.hashCode());
        result = prime * result + ((uuid == null)    ? 0 : uuid.hashCode());
        result = prime * result + ((city == null)    ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Server other = (Server) obj;
        if (name == null) {
            if (other.name != null) return false;
        }
        else if (!name.equals(other.name)) {
            return false;
        }

        if (uuid == null) {
            if (other.uuid != null) return false;
        }
        else if (!uuid.equals(other.uuid)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return name + " " + getFormattedLocation();
    }
}