package at.alladin.rmbt.android.preferences;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import at.alladin.rmbt.android.util.ConfigHelper;
import at.alladin.rmbt.android.util.Server;
import cz.nic.netmetr.R;

public class RMBTServerSelectFragment extends Fragment {
    public interface OnServerSelectedListener {
        void onServerSelected(Server server);
    }

    private OnServerSelectedListener listener;
    private Server selectedServer;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnServerSelectedListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.server_select_fragment, container, false);

        ListView listView = (ListView) view.findViewById(R.id.server_select_list);

        Set<String> serversSet = ConfigHelper.getServers(this.getActivity());

        ArrayList<Server> servers = new ArrayList<Server>();

        for(String serverString : serversSet) {
            servers.add(Server.decodeFromString(serverString));
        }

        this.selectedServer = ConfigHelper.getUserSelectedServer(getActivity());

        ServerSelectItemAdapter serverSelectItemAdapter = new ServerSelectItemAdapter(this.getActivity(), servers);
        listView.setAdapter(serverSelectItemAdapter);

        Button autoButton = (Button) view.findViewById(R.id.server_select_auto_button);
        autoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null) listener.onServerSelected(null);
            }
        });

        return view;
    }

    private class ServerSelectItemAdapter extends ArrayAdapter<Server> {

        public ServerSelectItemAdapter(Context context, ArrayList<Server> servers) {
            super(context, 0, servers);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final Server server = getItem(position);

            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.server_select_item, parent, false);
            }

            TextView serverName = (TextView) convertView.findViewById(R.id.serverName);
            TextView serverLocation = (TextView) convertView.findViewById(R.id.serverLocation);

            serverName.setText(server.getName());
            serverLocation.setText(server.getFormattedLocation());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if(listener != null) listener.onServerSelected(server);
                }
            });

            if(selectedServer != null && selectedServer.equals(server)) {
                ImageView selectedIcon = (ImageView) convertView.findViewById(R.id.serverSelectedIcon);
                selectedIcon.setVisibility(View.VISIBLE);
            }

            return convertView;
        }
    }
}