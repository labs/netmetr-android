/*******************************************************************************
 * Copyright 2013-2015 alladin-IT GmbH
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package at.alladin.rmbt.android.preferences;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import at.alladin.rmbt.android.main.FeatureConfig;
import at.alladin.rmbt.android.sync.RMBTSyncActivity;
import at.alladin.rmbt.android.terms.RMBTCheckFragment.CheckType;
import at.alladin.rmbt.android.terms.RMBTTermsActivity;
import at.alladin.rmbt.android.util.ConfigHelper;
import at.alladin.rmbt.android.util.Server;
import cz.nic.netmetr.R;

public class RMBTPreferenceActivity extends PreferenceActivity  {
    public static final String RESULT_REINIT_APP_EXTRA_KEY = "reinit_app";

    protected static final int REQUEST_NDT_CHECK = 1;
    protected static final int REQUEST_IC_CHECK = 2;
    protected static final int REQUEST_SERVER_SELECT = 3;
    
    protected Method mLoadHeaders = null;
    protected Method mHasHeaders = null;
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case android.R.id.home:
    		finish();
    		break;
    	}
    	
    	return super.onOptionsItemSelected(item);
    }
    
    /**
     * Checks to see if using new v11+ way of handling PrefsFragments.
     * 
     * @return Returns false pre-v11, else checks to see if using headers.
     */
    public boolean isNewV11Prefs()
    {
        if (mHasHeaders != null && mLoadHeaders != null)
            try
            {
                return (Boolean) mHasHeaders.invoke(this);
            }
            catch (final IllegalArgumentException e)
            {
            }
            catch (final IllegalAccessException e)
            {
            }
            catch (final InvocationTargetException e)
            {
            }
        return false;
    }
    
    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        // onBuildHeaders() will be called during super.onCreate()
        try
        {
            mLoadHeaders = getClass().getMethod("loadHeadersFromResource", int.class, List.class);
            mHasHeaders = getClass().getMethod("hasHeaders");
        }
        catch (final NoSuchMethodException e)
        {
        }
        super.onCreate(savedInstanceState);
        if (!isNewV11Prefs())
        {
            addPreferencesFromResource(R.xml.preferences);
            if (ConfigHelper.isDevEnabled(this))
                addPreferencesFromResource(R.xml.preferences_dev);
        }
        
        
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setDisplayHomeAsUpEnabled(true);
       
        final ListView v = getListView();
        v.setCacheColorHint(0);

        final Preference ndtPref = findPreference("ndt");
        if (ndtPref != null)
        {
            ndtPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    if (preference instanceof CheckBoxPreference)
                    {
                        final CheckBoxPreference cbp = (CheckBoxPreference) preference;
                        
                        if (cbp.isChecked())
                        {
                            cbp.setChecked(false);
                            final Intent intent = new Intent(getBaseContext(), RMBTTermsActivity.class);
                            intent.putExtra(RMBTTermsActivity.EXTRA_KEY_CHECK_TYPE, CheckType.NDT.name());
                            startActivityForResult(intent, REQUEST_NDT_CHECK);
                        }
                    }
                    return true;
                }
            });
        }
        
        final Preference icPref = findPreference("information_commissioner");
        if (icPref != null)
        {
        	if (FeatureConfig.TEST_USE_PERSONAL_DATA_FUZZING) {
	        	icPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	                @Override
	                public boolean onPreferenceClick(Preference preference)
	                {
	                    if (preference instanceof CheckBoxPreference)
	                    {
	                        final CheckBoxPreference cbp = (CheckBoxPreference) preference;
	                        
	                        if (cbp.isChecked())
	                        {
	                            cbp.setChecked(false);
	                            final Intent intent = new Intent(getBaseContext(), RMBTTermsActivity.class);
	                            intent.putExtra(RMBTTermsActivity.EXTRA_KEY_CHECK_TYPE, CheckType.INFORMATION_COMMISSIONER.name());
	                            startActivityForResult(intent, REQUEST_IC_CHECK);
	                        }
	                    }
	                    return true;
	                }
	            });
        	}
        	else {
        		final PreferenceCategory cat = (PreferenceCategory) findPreference("preference_category_test");
        		cat.removePreference(icPref);
        	}
        }
        
        final Preference gpsPref = findPreference("location_settings");
        if (gpsPref != null)
        {
            gpsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    return true;
                }
            });
        }
        
        final Preference syncPref = findPreference("sync_settings");
        if (syncPref != null)
        {
            syncPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    startActivity(new Intent(getBaseContext(), RMBTSyncActivity.class));
                    return true;
                }
            });
        }

        final Preference loop_mode = findPreference("loop_mode");
        if (loop_mode != null) {
            loop_mode.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ((CheckBoxPreference) findPreference("signal_only")).setChecked(false);
                    return true;
                }
            });
        }

        //Enable Development options
        addPreferencesFromResource(R.xml.preferences_dev);

        refreshServerSelectionState(true);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_NDT_CHECK) {
            ((CheckBoxPreference) findPreference("ndt")).setChecked(ConfigHelper.isNDT(this));
        }
        else if (requestCode == REQUEST_IC_CHECK) {
        	((CheckBoxPreference) findPreference("information_commissioner")).setChecked(ConfigHelper.isInformationCommissioner(this));
        }
        else if (requestCode == REQUEST_SERVER_SELECT) {
            refreshServerSelectionState(false);
        }

        Intent i  = new Intent();
        i.putExtra(RESULT_REINIT_APP_EXTRA_KEY, true);
        setResult(RESULT_OK, i);
    }

    private void refreshServerSelectionState(boolean setClickListener) {
        final Preference serverSelectButton = findPreference("user_server_selection_button");

        if(serverSelectButton == null) {
            Log.i(getClass().getSimpleName(), "Cannot refresh select button state, the button is null!");
            return;
        }

        Server server = ConfigHelper.getUserSelectedServer(this);
        if (server != null) {
            serverSelectButton.setSummary(server.toString());
        } else {
            serverSelectButton.setSummary(getText(R.string.preferences_user_server_selection_auto));
        }


        if(setClickListener) {
            serverSelectButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivityForResult(new Intent(getBaseContext(), RMBTServerSelectActivity.class), REQUEST_SERVER_SELECT); //Start for request code and refresh on result
                    return true;
                }
            });
        }
    }
}