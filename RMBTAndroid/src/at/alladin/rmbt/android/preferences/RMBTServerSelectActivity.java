package at.alladin.rmbt.android.preferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import at.alladin.rmbt.android.main.RMBTMainActivity;
import at.alladin.rmbt.android.util.ConfigHelper;
import at.alladin.rmbt.android.util.Server;
import cz.nic.netmetr.R;

public class RMBTServerSelectActivity extends Activity implements RMBTServerSelectFragment.OnServerSelectedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_select);
    }

    @Override
    public void onServerSelected(Server server) {
        if(server == null) {
            ConfigHelper.removeUserSelectedServer(getBaseContext());
        } else {
            ConfigHelper.setUserSelectedServer(getBaseContext(), server);
        }

        Toast.makeText(this, getText(R.string.user_server_selected), Toast.LENGTH_SHORT).show();

        setResult(Activity.RESULT_OK);
        finish();
    }
}
