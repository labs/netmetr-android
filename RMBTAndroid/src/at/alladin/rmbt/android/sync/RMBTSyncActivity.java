package at.alladin.rmbt.android.sync;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import at.alladin.rmbt.android.main.AppConstants;
import cz.nic.netmetr.R;

public class RMBTSyncActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        FragmentTransaction ft;
        ft = getFragmentManager().beginTransaction();

        final Fragment fragment = new RMBTSyncFragment();
        ft.add(R.id.fragment_content, fragment, AppConstants.PAGE_TITLE_SYNC);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        ft.commit();
    }
}
