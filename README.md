Netmetr Android
===============

Android client application for NetMetr.

Dependencies
---------------

The following third party libraries are required dependencies:

### Google Play Services ###

- see <http://developer.android.com/google/play-services/setup.html>.
- copy "extras/google/google_play_services/libproject" as "google-play-services_lib" into the source distribution of *Open-RMBT*.


### Android Support Library ###

- see <http://developer.android.com/tools/extras/support-library.html>
- copy as "RMBTAndroid/libs/android-support-v13.jar"


### JSON in Java ###

- MIT License (+ "The Software shall be used for Good, not Evil.")
- available at <http://www.json.org/java/index.html>
- copy as "RMBTSharedCode/lib/org.json.jar" and "RMBTClient/lib/org.json.jar"


### Simple Logging Facade for Java (SLF4J) ###

- MIT License
- available at <http://www.slf4j.org/>
- copy as "RMBTAndroid/libs/slf4j-android-1.5.8.jar"


### JOpt Simple ###

- MIT License
- available at <http://pholser.github.com/jopt-simple/>
- copy as "RMBTClient/lib/jopt-simple-3.2.jar"