package cz.nic.netmetr.clientSecret;

import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;

public class ClientSecretHelper {

    public static boolean checkClientSecret(String clientSecretHash, String uuid, String clientName, String versionName, int versionCode, long time, String clientSecret) {
        if (isEmptyString(clientSecretHash)) {
            return false;
        }

        long currentTimeMillis = System.currentTimeMillis();

        Calendar remoteTime = Calendar.getInstance();
        remoteTime.setTime(new Date(time));

        Calendar beforeTime = Calendar.getInstance();
        beforeTime.setTime(new Date(currentTimeMillis));
        beforeTime.add(Calendar.HOUR, -2);

        Calendar afterTime = Calendar.getInstance();
        afterTime.setTime(new Date(currentTimeMillis));
        afterTime.add(Calendar.HOUR, 1);

        boolean timeCheck = beforeTime.before(remoteTime) && afterTime.after(remoteTime);

        return clientSecretHash.equals(prepareClientSecret(uuid, clientName, versionName, versionCode, time, clientSecret)) && timeCheck;
    }

    public static String prepareClientSecret(String uuid, String clientName, String versionName, int versionCode, long time, String clientSecret) {
        String clientSecretSource = String.format("%s_%s_%s_%s_%s_%s", new Object[] {uuid, clientName, versionName, Integer.valueOf(versionCode), Long.valueOf(time), clientSecret});
        return calculateSha256Hash(clientSecretSource);
    }

    private static String calculateSha256Hash(String input) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private static boolean isEmptyString(String str) {
        return str == null || str.length() == 0;
    }

}
